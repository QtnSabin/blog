<?php
session_start();
?>

<!DOCTYPE HTML>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="assets/style/style.php" type="text/css">
    <?php
    //si pas de param
    if((empty($_REQUEST) && (!isset($_SESSION['auth']) || isset($_SESSION['auth']) && $_SESSION['auth'] == false)) || (isset($_REQUEST['page']) && $_REQUEST['page'] == 'logout'))
    {
        include_once ('templates/summary/meta.html');
    }
    elseif(isset($_REQUEST['page']))
    {
        switch ($_REQUEST['page'])
        {
            case 'login':
                include_once ('templates/connexion/meta.html');
                break;
            default:
                include_once ('404meta.html');
                break;
        }
    }
    elseif(isset($_SESSION['auth']))
    {
        include_once ('templates/articles/meta.html');
    }
    ?>
</head>
<body>
    <?php
    include_once ('templates/header.php');

    // si pas de param et pas login
    if(empty($_REQUEST) && (!isset($_SESSION['auth']) || isset($_SESSION['auth']) && $_SESSION['auth'] == false))
    {
        include_once ('templates/summary/body.php');
    }
    // si param
    elseif(isset($_REQUEST['page']))
    {
        switch ($_REQUEST['page'])
        {
            case 'login':
                include_once ('templates/connexion/body.html');
                break;
            case 'logout':
                include_once ('templates/connexion/logout.php');
                break;
            default:
                include_once ('404.html');
                break;
        }
    }
    elseif(isset($_SESSION['auth']))
    {
        include_once ('templates/articles/articles.php');
    }

    include_once ('templates/footer.php')
    ?>
</body>
</html>