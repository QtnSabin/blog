<?php
header('content-type: text/css');
header('Cache-Control: max-age=3600, must-revalidate');
session_start();

$config = file('../../models/config.txt');
//0->Taille h1
//1->Couleur h1
//2->Police h1
?>
*
{
    margin: 0;
    padding: 0;
}

main
{
    display: flex;
    align-content: stretch;
    justify-content: stretch;
    flex-direction: column;
}

section
{
    display: flex;
    align-content: stretch;
    justify-content: stretch;
    flex-direction: column;
}

article
{
    display: inline-flex;
    align-content: stretch;
    justify-content: stretch;
    flex-direction: column;
}

h2
{
font-size : <?php echo $config['0']; ?>;
color : <?php echo $config['1']; ?>;
font-family : <?php echo $config['2']; ?>;
}

p
{
font-size : <?php echo $config['3']; ?>;
color : <?php echo $config['4']; ?>;
font-family : <?php echo $config['5']; ?>;
}
