<?php

    $dsn = 'mysql:host=localhost;dbname=infoblog;charset=utf8';
    $user = 'root';
    $pwd = '';

    try
    {
        $cnx = new PDO($dsn, $user, $pwd);
        $cnx->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch(PDOException $e)
    {
        echo 'Un problème est survenu !';
    }

?>
