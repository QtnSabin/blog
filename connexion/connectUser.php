<?php
    session_start();
    $_SESSION['auth'] = false;

    require 'connexion.php';

    $login = $_REQUEST['login'];
    $algo = PASSWORD_BCRYPT;
    $password = $_REQUEST['password'];

    $sql = "SELECT * FROM users WHERE login='$login'";
    $data = $cnx->query($sql)->fetch();

    if($data) {
        $hashed_password = $data['password'];

        if (password_verify($password, $hashed_password)) {
            echo "Mot de passe correct !";
            $_SESSION['auth'] = true;
            $_SESSION['id'] = $data['ID'];
            $_SESSION['role'] = $data['roles_id'];

            //setcookie('true', '');
            header('Location: ../index.php');
            exit();
        } else {
            echo "Mot de passe incorrecte !";
        }
    }else{
        echo 'Login incorrecte !';
    }
?>
