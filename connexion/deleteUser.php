<?php

    require 'connexion.php';

    $id = $_REQUEST['id'];
    $sql = "DELETE FROM users WHERE id=?";
    $rs_delete = $cnx->prepare($sql);

    try{
        $rs_delete->bindValue(1, $id, PDO::PARAM_INT);

        $rs_delete->execute();
        echo "Suppression réussie<br /><a href='index.php'>Retour</a>";
    }
    catch(PDOException $e)
    {
        echo "Erreur de suppression.<br /><a href='index.php'>Merci de réessayer</a>";
    }

?>