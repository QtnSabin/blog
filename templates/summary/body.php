<?php
include_once ('connexion/connexion.php');
$sql1 = 'SELECT Name FROM categories ORDER BY Name';
$sql2 = 'SELECT Title, CreationDate FROM articles ORDER BY CreationDate ASC LIMIT 5';
$result1 = $cnx->query($sql1)->fetchAll();
$result2 = $cnx->query($sql2)->fetchAll();

echo('<h2>Toutes les catégories</h2>');
foreach ($result1 as $row)
{
    echo('<p>'.$row['Name'].'</p>');
}

echo('<h2>Les 5 derniers articles</h2>');
foreach ($result2 as $row)
{
    echo('<p>'.$row['Name'].'</p>');
}