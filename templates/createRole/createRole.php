<?php
    require('../../connexion/connexion.php');

    $sql = "INSERT INTO roles (ID, Name) VALUES (null, :name)";
    $rs_insert = $cnx->prepare($sql);

    try
    {
        $rs_insert->bindValue(':name', $_REQUEST['name'], PDO::PARAM_STR);

        $rs_insert->execute();
        echo 'Role créé !';
    }
    catch(PDOException $e)
    {
        echo 'Erreur d\'insertion, merci de recommencer !';
    }

?>
