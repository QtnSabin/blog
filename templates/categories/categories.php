<?php
require('../../connexion/connexion.php');

$sql = "SELECT a.id, a.name from categories a";
//$sql2 = "SELECT a.id, a.title from articles a"; // recuperation id + titre artcile
$categories = $cnx->query($sql)->fetchAll();
//$articles = $cnx->query($sql2)->fetchAll(); // attribution d'une variable à artcile
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Categories</title>
    <link href="../../assets/general.css" rel="stylesheet">
</head>
<body>
<div id="categories">
    <p id="message">Tableau des catégories</p>
    <table>
        <thead>
        <tr>
            <th>ID</th>
            <th>Nom</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($categories as $categories) { ?>
            <tr>
                <td><?= $categories['id'] ?></td>
                <td><?= $categories['name'] ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
</body>
</html>

