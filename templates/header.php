<header>
    <h1>Infoblog</h1>
    <?php
    if(!isset($_SESSION['auth']) || $_SESSION['auth'] == false || (isset($_REQUEST['page']) && $_REQUEST['page']))
    {
        echo('<a href="?page=login"><button>Connexion</button></a>');
    }
    else
    {
        echo('<a href="?page=logout"><button>Déconnexion</button></a>');
    }
    ?>
</header>