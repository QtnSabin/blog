<?php
    include_once ('connexion/connexion.php');

    $sql = "SELECT a.id, a.title, a.creationDate, u.login from articles a 
        INNER JOIN users u ON u.id = a.createUserId";
    $articles = $cnx->query($sql)->fetchAll();
?>

<div id="articles">
    <p id="message"></p>
    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>TITRE</th>
                <th>CRÉATEUR</th>
                <th>DATE DE CRÉATION</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($articles as $article) { ?>
                <tr>
                    <td><?= $article['id'] ?></td>
                    <td><?= $article['title'] ?></td>
                    <td><?= $article['login'] ?></td>
                    <td><?= date('Y/m/d à H:i', strtotime($article['creationDate'])) ?></td>
                    <td><button onclick="deleteArticle(<?= $article['id'] ?>)" id="<?= 'delete-' . $article['id'] ?>">Supprimer</button></td>
                    <td><button>Modifier</button></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    function deleteArticle(id) {
        $.ajax({
            type: 'POST',
            url: 'templates/articles/deleteArticle.php',
            data: {
                id : id,
            },
            success: function(result){
                console.log(result);
                switch (result) {
                    case 'error':
                        $("#message").html("Erreur lors de la suppression.");
                        $("#message").css("color", "red");
                        break;
                    case 'success':
                        location.reload();
                        break;
                }
            }
        });
    }
</script>