<?php
    session_start();

    require('../../connexion/connexion.php');
    require_once('../../config.php');
    date_default_timezone_set('Europe/Paris');

    $sql = "INSERT INTO articles (ID, Title, Content, Image, CreationDate, ModificationDate, CreateUserID, UpdateUserID) 
        VALUES (null, :title, :content, :image, :creationDate, null, :createUserID, null)";
    $rs_insert = $cnx->prepare($sql);

    if ($_SESSION['auth'] && $_SESSION['role'] == $adminRole) {
        if (isset($_REQUEST['title']) && $_REQUEST['title'] != '') {
            if (isset($_REQUEST['content']) && $_REQUEST['content'] != '') {
                try {
                    $rs_insert->bindValue(':title', $_REQUEST['title'], PDO::PARAM_STR);
                    $rs_insert->bindValue(':content', $_REQUEST['content'], PDO::PARAM_STR);
                    $rs_insert->bindValue(':image', null);
                    $rs_insert->bindValue(':creationDate', date("Y-m-d H:i:s"));
                    $rs_insert->bindValue(':createUserID', $_SESSION['id']);
                    $rs_insert->execute();
                    $response = 'success';
                }
                catch(PDOException $e)
                {
                    $response = 'error';
                }
            } else {
                $response = 'content';
            }
        } else {
            $response = 'title';
        }
    } else {
        $response = 'connexion';
    }

    echo $response;
?>