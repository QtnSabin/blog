<?php
require_once '../../connexion/connexion.php';

    $id = $_REQUEST['id'];
    $sql = "DELETE FROM articles WHERE id=?";
    $rs_delete = $cnx->prepare($sql);

    try{
        $rs_delete->bindValue(1, $id, PDO::PARAM_INT);

        $rs_delete->execute();
        echo "success";
    }
    catch(PDOException $e)
    {
        echo "error";
    }
?>