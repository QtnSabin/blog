<?php
    require ('../../connexion/connexion.php');

    $login = $_POST['login'];
    $password = $_POST['password'];//crypt($_POST['password']);
    $algo = PASSWORD_BCRYPT;
    $password = password_hash($password, $algo);

    $sql = "INSERT INTO users (id, login, password, roles_id) VALUES (NULL, :login, :password, 1)";
    $rs_insert = $cnx->prepare($sql);

    try
    {
        $rs_insert->bindValue(':login', $login, PDO::PARAM_STR);
        $rs_insert->bindValue(':password', $password, PDO::PARAM_STR);
        //$rs_insert->bindValue(':role', $_REQUEST['role'], PDO::PARAM_INT);

        $rs_insert->execute();
        echo 'Utilisateur créé !';
    }
    catch(PDOException $e)
    {
        echo 'Erreur d\'insertion, merci de recommencer !';
    }

?>
