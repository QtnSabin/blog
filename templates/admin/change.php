<?php
    $h1Size = $_POST["h1-size"]."\n";
    $h1Color = $_POST["h1-color"]."\n";
    $h1Font = $_POST["h1-font"]."\n";
    $pSize = $_POST["p-size"]."\n";
    $pColor = $_POST["p-color"]."\n";
    $pFont = $_POST["p-font"]."\n";

    // Ecriture du fichier de config
    $myfile = fopen("../../models/config.txt", "w") or die("Unable to open file!");
    $txt = $h1Size.$h1Color.$h1Font.$pSize.$pColor.$pFont;
    fwrite($myfile, $txt);
    fclose($myfile);

    header("Location: ../../demo.html");
?>
