<main>
    <?php

        echo "<script src='../../assets/js/ajax.min.js'></script>
              <script src='../../assets/js/jquery.min.js'></script>
              <section><h2>Créer un rôle</h2>";
        include_once ("../createRole/createRole.html");

        echo "</section><section><h2>Créer un utilisateur</h2>";
        include_once ("../createUser/createUser.html");

        echo "</section><section><h2>Créer un article</h2>";
        include_once ("../articles/createArticle.html");
        echo "</section>"
    ?>

    <section>
        <form method="post" action="change.php">

            <label for="h1-size">Taille des titres H1:</label>
            <select id="h1-size" name="h1-size">
                <option value="2em">Petite</option>
                <option value="3em">Moyenne</option>
                <option value="4em">Grande</option>
            </select>

            <label for="h1-color">Couleur des titres H1:</label>
            <select id="h1-color" name="h1-color">
                <option value="blue">Bleu</option>
                <option value="green">Vert</option>
                <option value="pink">Rose</option>
            </select>

            <label for="h1-font">Police des titres H1:</label>
            <select id="h1-font" name="h1-font">
                <option value="Times New Roman">time new roman</option>
                <option value="Calibri">calibri</option>
                <option value="Lucida Console">lucida</option>
            </select>

            <label for="p-size">Taille des textes:</label>
            <select id="p-size" name="p-size">
                <option value="0.5em">Petite</option>
                <option value="0.8em">Moyenne</option>
                <option value="1.2em">Grande</option>
            </select>

            <label for="p-color">Couleur des titres textes:</label>
            <select id="p-color" name="p-color">
                <option value="blue">Bleu</option>
                <option value="green">Vert</option>
                <option value="pink">Rose</option>
            </select>

            <label for="p-font">Police des textes:</label>
            <select id="p-font" name="p-font">
                <option value="Times New Roman">time new roman</option>
                <option value="Calibri">calibri</option>
                <option value="Lucida Console">lucida</option>
            </select>

            <button type="submit">Soumettre</button>

        </form>
    </section>

</main>
